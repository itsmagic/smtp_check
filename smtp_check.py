import wx
import os
import sys
import smtp_check_gui
import json
import smtplib
import ssl


class SMTP_Check(smtp_check_gui.SMTPCheckFrame):
    def __init__(self, parent):
        smtp_check_gui.SMTPCheckFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "pn.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "SMTP Check"
        self.version = "v0.0.1"
        self.path = os.path.expanduser(os.path.join('~', 'Documents', self.name))
        self.SetTitle(f'{self.name} {self.version}')
        self.preload_settings()

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def on_text_update(self, event):
        """Check if all the fields are filled out an enable the send button, or not"""
        if len(self.server_txt.GetValue()) and \
           len(self.port_txt.GetValue()) and \
           len(self.sender_email_txt.GetValue()) and \
           len(self.password_txt.GetValue()) and \
           len(self.from_txt.GetValue()) and \
           len(self.to_txt.GetValue()) and \
           len(self.subject_txt.GetValue()) and \
           len(self.body_txt.GetValue()):
            self.send_btn.Enable(True)

        with open('settings.json', 'w') as f:
            my_settings = {
                "server_txt": self.server_txt.GetValue(),
                "port_txt": self.port_txt.GetValue(),
                "sender_email_txt": self.sender_email_txt.GetValue(),
                "from_txt": self.from_txt.GetValue(),
                "to_txt": self.to_txt.GetValue(),
                "subject_txt": self.subject_txt.GetValue(),
                "body_txt": self.body_txt.GetValue()
            }
            json.dump(my_settings, f)

    def preload_settings(self):
        if os.path.exists('settings.json'):
            with open('settings.json', 'r') as f:
                settings = json.load(f)
                for setting in settings.keys():
                    getattr(self, setting).SetValue(settings[setting])

    def on_send_btn(self, event):
        """ Send the email"""

        smtp_server = self.server_txt.GetValue()
        port = self.port_txt.GetValue()
        sender_email = self.sender_email_txt.GetValue()
        password = self.password_txt.GetValue()
        from_email = self.from_txt.GetValue()
        to_email = self.to_txt.GetValue()
        subject = self.subject_txt.GetValue()
        body = self.body_txt.GetValue()

        # Create a secure SSL context
        if self.use_tls_chk.GetValue():
            context = ssl.create_default_context()

        # Try to log in to server and send email
        try:
            self.add_to_logs(f'Attempting Connection to {smtp_server} on port:{port}\r\n')
            server = smtplib.SMTP(smtp_server, port)
            self.add_to_logs(f'Connected!\r\n')
            self.add_to_logs(str(server.ehlo()))
            if self.use_tls_chk.GetValue():
                self.add_to_logs(str(server.starttls(context=context)))
            self.add_to_logs(str(server.ehlo()))
            self.add_to_logs(str(server.login(sender_email, password)))
            msg = (f"Subject:{subject}\r\nFrom:{from_email}\r\nTo:{to_email}\n\n{body}")
            self.add_to_logs(str(server.sendmail(from_email, to_email, msg)))
            self.add_to_logs(str(server.quit()))
            return
        except Exception as e:
            # Print any error messages to stdout
            self.add_to_logs(repr(e))
        finally:
            try:
                server.quit()
            except Exception as error:
                self.log_txt.AppendText(repr(error))

    def on_clear_logs(self, event):
        self.log_txt.Clear()

    def add_to_logs(self, log):
        plain_txt = str(log) + '\r\n'
        self.log_txt.AppendText(plain_txt)

    def on_exit(self, event):
        self.Close()




def main():
    """run the main program"""
    sc_app = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    sc_frame = SMTP_Check(None)
    sc_frame.Show()
    sc_app.MainLoop()


if __name__ == '__main__':
    main()
