# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class SMTPCheckFrame
###########################################################################

class SMTPCheckFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Server Configuration" ), wx.VERTICAL )

		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText1 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Server", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )

		self.m_staticText1.SetMinSize( wx.Size( 80,-1 ) )

		bSizer3.Add( self.m_staticText1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.server_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.server_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer3.Add( self.server_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer1.Add( bSizer3, 0, wx.EXPAND, 5 )

		bSizer31 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText11 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )

		self.m_staticText11.SetMinSize( wx.Size( 80,-1 ) )

		bSizer31.Add( self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.port_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, u"587", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.port_txt.SetMaxSize( wx.Size( 60,-1 ) )

		bSizer31.Add( self.port_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.use_tls_chk = wx.CheckBox( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Use TLS", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.use_tls_chk.SetValue(True)
		self.use_tls_chk.Enable( False )

		bSizer31.Add( self.use_tls_chk, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer1.Add( bSizer31, 1, wx.EXPAND, 5 )

		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText2 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Sender Email", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )

		self.m_staticText2.SetMinSize( wx.Size( 80,-1 ) )

		bSizer4.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.sender_email_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.sender_email_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer4.Add( self.sender_email_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer1.Add( bSizer4, 0, wx.EXPAND, 5 )

		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		self.m_staticText3.SetMinSize( wx.Size( 80,-1 ) )

		bSizer5.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.password_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		self.password_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer5.Add( self.password_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer1.Add( bSizer5, 1, wx.EXPAND, 5 )


		bSizer2.Add( sbSizer1, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Email" ), wx.VERTICAL )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText4 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"From", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		self.m_staticText4.SetMinSize( wx.Size( 80,-1 ) )

		bSizer6.Add( self.m_staticText4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.from_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.from_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer6.Add( self.from_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer2.Add( bSizer6, 0, wx.EXPAND, 5 )

		bSizer61 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText41 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"To", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText41.Wrap( -1 )

		self.m_staticText41.SetMinSize( wx.Size( 80,-1 ) )

		bSizer61.Add( self.m_staticText41, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.to_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.to_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer61.Add( self.to_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer2.Add( bSizer61, 0, wx.EXPAND, 5 )

		bSizer62 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText42 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Subject", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText42.Wrap( -1 )

		self.m_staticText42.SetMinSize( wx.Size( 80,-1 ) )

		bSizer62.Add( self.m_staticText42, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.subject_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.subject_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer62.Add( self.subject_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer2.Add( bSizer62, 0, wx.EXPAND, 5 )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Body" ), wx.VERTICAL )

		self.body_txt = wx.TextCtrl( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP )
		self.body_txt.SetMinSize( wx.Size( 400,100 ) )

		sbSizer3.Add( self.body_txt, 1, wx.ALL|wx.EXPAND, 5 )


		sbSizer2.Add( sbSizer3, 1, wx.EXPAND|wx.ALL, 5 )

		self.send_btn = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Send", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.send_btn.Enable( False )

		sbSizer2.Add( self.send_btn, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )


		bSizer2.Add( sbSizer2, 1, wx.ALL|wx.EXPAND, 5 )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Logs" ), wx.VERTICAL )

		self.log_txt = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP )
		self.log_txt.SetMinSize( wx.Size( 400,200 ) )

		sbSizer4.Add( self.log_txt, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_button3 = wx.Button( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Clear Logs", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer4.Add( self.m_button3, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )


		bSizer2.Add( sbSizer4, 1, wx.EXPAND|wx.ALL, 5 )


		self.m_panel1.SetSizer( bSizer2 )
		self.m_panel1.Layout()
		bSizer2.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem1 )

		self.m_menubar1.Append( self.m_menu1, u"File" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.server_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.port_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.sender_email_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.password_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.from_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.to_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.subject_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.body_txt.Bind( wx.EVT_TEXT, self.on_text_update )
		self.send_btn.Bind( wx.EVT_BUTTON, self.on_send_btn )
		self.m_button3.Bind( wx.EVT_BUTTON, self.on_clear_logs )
		self.Bind( wx.EVT_MENU, self.on_exit, id = self.m_menuItem1.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_text_update( self, event ):
		event.Skip()








	def on_send_btn( self, event ):
		event.Skip()

	def on_clear_logs( self, event ):
		event.Skip()

	def on_exit( self, event ):
		event.Skip()


